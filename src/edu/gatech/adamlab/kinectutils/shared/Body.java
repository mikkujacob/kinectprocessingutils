package edu.gatech.adamlab.kinectutils.shared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.gatech.adamlab.kinectutils.shared.Constants.JPIDX;
import processing.core.PVector;

import java.io.Serializable;

public class Body extends HashMap<JPIDX, PVector> implements Serializable, Cloneable
{

	/**
	 * Serial Version UID - DO NOT CHANGE PLEASE!!!
	 */
	private static final long serialVersionUID = 2610605569934643630L;
	
	private float timestamp = 0.0f;

	public float getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(float timestamp)
	{
		this.timestamp = timestamp;
	}

	@Override
	public PVector get(Object jidxObject)
	{
		JPIDX jidx = (JPIDX) jidxObject;
		if (JPIDX.LEFT_COLLAR == jidx || JPIDX.RIGHT_COLLAR == jidx)
		{
			return super.get(JPIDX.NECK);
		}
		else if (JPIDX.WAIST == jidx)
		{
			return super.get(JPIDX.TORSO);
		}
		else if (JPIDX.LEFT_ANKLE == jidx)
		{
			return super.get(JPIDX.LEFT_FOOT);
		}
		else if (JPIDX.RIGHT_ANKLE == jidx)
		{
			return super.get(JPIDX.RIGHT_FOOT);
		}
		else
		{
			return super.get(jidx);
		}
	}

	public void set(JPIDX iJidx, PVector iPvector)
	{
		put(iJidx, iPvector);
	}

	public void initialize(HashMap<Integer, Float[]> joints, float timestamp)
	{
		setTimestamp(timestamp);
		
		for(Entry<Integer, Float[]> entry : joints.entrySet())
		{
			Float[] vector = entry.getValue();
			set(JPIDX.values()[entry.getKey()], new PVector(vector[0], vector[1], vector[2]));
		}
	}

	public PVector[] localBasis()
	{
		PVector localFwddir = PVecUtilities.orthonormalization(orientation(),
				PVecUtilities.UPVEC);
		PVector localSidedir = PVecUtilities.UPVEC.cross(localFwddir);
		PVector[] localBasis =
		{ localSidedir, PVecUtilities.UPVEC, localFwddir };
		return localBasis;
	}

	public PVector center()
	{
		return get(JPIDX.TORSO);
	}

	public Body transformed(PVector[] sourceBasis, PVector[] targetBasis,
			PVector targetCenter)
	{
		Body transformedPose = new Body();
		PVector center = center();
		for (JPIDX jidx : JPIDX.ALL_JPIDX)
		{
			if (JPIDX.TORSO == jidx)
			{
				transformedPose.set(JPIDX.TORSO, targetCenter);
			}
			else
			{
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				float[] dissolution = PVecUtilities.dissolve(radiusVector,
						sourceBasis);
				PVector normalizedPosition = PVecUtilities.assemble(
						targetBasis, dissolution);
				transformedPose.set(jidx,
						PVector.add(targetCenter, normalizedPosition));
			}
		}
		transformedPose.setTimestamp(getTimestamp());
		return transformedPose;
	}

	public Body translated(PVector targetCenter)
	{
		Body translatedPose = new Body();
		PVector center = center();
		for (JPIDX jidx : JPIDX.ALL_JPIDX)
		{
			if (JPIDX.TORSO == jidx)
			{
				translatedPose.set(jidx, targetCenter);
			}
			else
			{
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				translatedPose.set(jidx,
						PVector.add(targetCenter, radiusVector));
			}
		}
		return translatedPose;
	}

	public PVector orientation()
	{
		return PVecUtilities.normTo3Pt(get(JPIDX.TORSO),
				get(JPIDX.LEFT_SHOULDER), get(JPIDX.RIGHT_SHOULDER));
	}

	public PVector upperUpvec()
	{
		PVector upvec = PVector.sub(get(JPIDX.NECK), get(JPIDX.TORSO));
		upvec.div(upvec.mag());
		return upvec;
	}

	public PVector lowerUpvec()
	{
		return PVecUtilities.UPVEC;
	}

	@Override
	public Object clone()
	{
		Body cln = new Body();

		cln.setTimestamp(getTimestamp());

		for (JPIDX jidx : keySet())
		{
			cln.set(jidx, PVecUtilities.clone(get(jidx)));
		}

		return cln;
	}

	// interpolates a position of the body from the positions and their
	// coefficients given in parameter
	public static Body interpolate(Body body1, Body body2, float coefficient)
	{

		// position of the body to be returned
		Body body = new Body();

		// to store the part of the body which position is being computed
		JPIDX iJidx;

		// browses all parts of the body
		for (Map.Entry<JPIDX, PVector> joints_iterator : body1.entrySet())
		{

			// part of the body to interpolate
			iJidx = joints_iterator.getKey();

			// interpolates the position of this part of the body and stores it
			body.set(iJidx, PVector.lerp(joints_iterator.getValue(),
					body2.get(iJidx), coefficient));

		}

		// the interpolated position of the body is returned
		return body;

	}
	
	// Calculates the centroid of the body from all bodies in an ArrayList<Body>
	public static Body centroid(ArrayList<Body> bodies)
	{
		// position of the body to be returned
		Body body = (Body)bodies.get(0).clone();

		// to store the part of the body which position is being computed
		JPIDX iJidx;

		for(int i = 1; i < bodies.size(); i++)
		{
			Body body1 = bodies.get(i);
			// browses all parts of the body
			for (Map.Entry<JPIDX, PVector> joints_iterator : body1.entrySet())
			{
				//Incremental average
				//mt = mt-1 + (at - mt-1) / t
				// part of the body to centroid
				iJidx = joints_iterator.getKey();
				PVector prototypeJoint = body.get(iJidx);
				PVector currentJoint = body1.get(iJidx);
				
				body.set(iJidx, PVector.add(prototypeJoint, PVector.div(PVector.sub(currentJoint, prototypeJoint), i)).get());
			}
		}

		// the centroid position of the body is returned
		return body;

	}

	public boolean isSimilar(Body other, float relativeDeviationTolerance)
	{
		PVector centerA = get(JPIDX.TORSO);
		PVector centerB = other.get(JPIDX.TORSO);
		for (JPIDX jidx : JPIDX.ALL_JPIDX)
		{
			if (JPIDX.TORSO == jidx)
				continue;
			PVector radialA = PVector.sub(get(jidx), centerA);
			PVector radialB = PVector.sub(other.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			float referenceMag = Math.max(radialA.mag(), radialB.mag());
			float deviationMag = deviation.mag();
			if (deviationMag > referenceMag * relativeDeviationTolerance)
			{
				return false;
			}
		}
		return true;
	}
	
	public float difference(Body other)
	{
		PVector centerA = get(JPIDX.TORSO);
		PVector centerB = other.get(JPIDX.TORSO);
		float deviationMag = 0f;
		for (JPIDX jidx : JPIDX.ALL_JPIDX)
		{
			if (JPIDX.TORSO == jidx)
				continue;
			PVector radialA = PVector.sub(get(jidx), centerA);
			PVector radialB = PVector.sub(other.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			deviationMag += Math.abs(deviation.mag());
		}
		return deviationMag;
	}

	public float characteristicSize()
	{
		float sumsize = 0.0f;

		for (Pair<JPIDX, JPIDX> jidxpair : LIDX.LIMB_ENDS().values())
		{
			sumsize += PVector.sub(get(jidxpair.second), get(jidxpair.first)).mag();
		}

		return sumsize / LIDX.LIMB_ENDS().size();
	}

	public Body scale(float factor)
	{
		Body scaled = new Body();

		for (JPIDX jidx : JPIDX.ALL_JPIDX)
		{
			scaled.set(jidx, PVector.mult(get(jidx), factor));
		}

		scaled.setTimestamp(getTimestamp());
		return scaled;
	}
	
	public String toString()
	{
		String text = "[Body: [Timestamp: " + getTimestamp() + ", Joints: [";
		JPIDX[] jointIndices = JPIDX.ALL_JPIDX;
		text += "" + jointIndices[0].toString() + ": " + get(jointIndices[0]).toString();
		for(int index = 1; index < jointIndices.length; index++)
		{
			text += ", " + jointIndices[index].toString() + ": " + get(jointIndices[index]).toString();
		}
		
		text += "]]]";
		return text;
	}
	
	public static Body getIdealUserPose()
	{
		Body idealUserPose = new Body();
		//Set Skeleton Coordinates for ideal user Body
		idealUserPose.set(JPIDX.HEAD,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JPIDX.NECK,new PVector(49.31178f, 482.05545f, 1871.1399f));
		idealUserPose.set(JPIDX.LEFT_SHOULDER,new PVector(-117.19889f, 473.39166f, 1895.8171f));
		idealUserPose.set(JPIDX.LEFT_ELBOW,new PVector(-234.26807f, 214.76712f, 1940.3701f));
		idealUserPose.set(JPIDX.LEFT_HAND,new PVector(-502.1734f, 154.27269f, 1752.4586f));
		idealUserPose.set(JPIDX.LEFT_FINGERTIP,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JPIDX.RIGHT_SHOULDER,new PVector(215.82245f, 490.7192f, 1846.4626f));
		idealUserPose.set(JPIDX.RIGHT_ELBOW,new PVector(380.55743f, 234.44539f, 1845.75f));
		idealUserPose.set(JPIDX.RIGHT_HAND,new PVector(641.58655f, 210.31554f, 1670.9052f));
		idealUserPose.set(JPIDX.RIGHT_FINGERTIP,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JPIDX.TORSO,new PVector(56.47932f, 240.32893f, 1834.6368f));
		idealUserPose.set(JPIDX.LEFT_HIP,new PVector(-43.330074f, -6.9635806f, 1813.9879f));
		idealUserPose.set(JPIDX.LEFT_KNEE,new PVector(-31.571592f, -493.167f, 1824.4634f));
		idealUserPose.set(JPIDX.LEFT_FOOT,new PVector(18.669317f, -967.7855f, 1913.0208f));
		idealUserPose.set(JPIDX.RIGHT_HIP,new PVector(170.6238f, 4.1684284f, 1782.2798f));
		idealUserPose.set(JPIDX.RIGHT_KNEE,new PVector(193.66336f, -464.59735f, 1739.4097f));
		idealUserPose.set(JPIDX.RIGHT_FOOT,new PVector(221.68254f, -936.3788f, 1838.4629f));
		idealUserPose = idealUserPose.translated(new PVector(0, 0, idealUserPose.center().z));
		return idealUserPose;
	}
}
