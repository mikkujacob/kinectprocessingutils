package edu.gatech.adamlab.kinectutils.shared;

public class DataTypeConversions
{
	public static double[] intArraytoDoubleArray(int[] ints)
	{
		double[] doubles = new double[ints.length];
		int i = 0;
		for(int o : ints)
		{
			doubles[i++] = (double) o;
		}
		
		return doubles;
	}
	
	
}
