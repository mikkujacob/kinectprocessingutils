package edu.gatech.adamlab.kinectutils.shared;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class TempoIndependentMovement implements Serializable
{

	/**
	 * Generated serial ID of this class
	 */
	private static final long serialVersionUID = 3878378678603076421L;

	// tree map storing body positions and the time this body has been recorded
	private TreeMap<Float, Body> bodies = new TreeMap<Float, Body>();

	// relative acceleration of the replay against the original scene
	private float replaySpeed = 1;

	// whether the replay has to be repeated or not
	// if yes, it is repeated backward once finished until it is back to the
	// beginning
	private boolean isRepetitionOn = true;

	// if true, always repeat from the beginning to the end
	// if false, play from the beginning to the end, then from the end to the
	// beginning, then from the beginning to the end, etc.
	private boolean isDirectRepetition = false;

	private float duration = 0.0f;

	// total time elapsed since the beginning of the recording or the replay
	private float totalTime = 0.0f;

	private float percentPassed = 0.0f;
	
	//Globally unique ID.
	private UUID ID;

	public TempoIndependentMovement(ArrayList<Body> bodies)
	{
		ID = UUID.randomUUID();
		if (bodies != null && !bodies.isEmpty())
		{
			float startingTime = bodies.get(0).getTimestamp();

			for (Body body : bodies)
			{
				this.bodies.put(body.getTimestamp() - startingTime, body);
			}

			duration = bodies.get(bodies.size() - 1).getTimestamp()
					- bodies.get(0).getTimestamp();
		}
	}

	public void setReplaySpeed(float replaySpeed)
	{
		this.replaySpeed = replaySpeed;
	}

	public float getReplaySpeed()
	{
		return this.replaySpeed;
	}

	// updates whether the replay has to be repeated or not
	public void setIsRepetitionOn(boolean isRepetitionOn)
	{
		this.isRepetitionOn = isRepetitionOn;
	}

	public void setDirectRepetition(boolean isDirectRepetition)
	{
		this.isDirectRepetition = isDirectRepetition;
	}

	public boolean isRepetitionOn()
	{
		return this.isRepetitionOn;
	}

	public boolean isDirectRepetition()
	{
		return this.isDirectRepetition;
	}

	// sets the speed back to its original value
	public void reinitializeSpeed()
	{
		replaySpeed = 1;
	}

	public Body at(float time)
	{
		if (time >= bodies.lastKey())
		{
			return (Body) bodies.lastEntry().getValue().clone();
		}
		else if (time <= bodies.firstKey())
		{
			return (Body) bodies.firstEntry().getValue().clone();
		}
		else
		{
			Map.Entry<Float, Body> floorEntry = bodies.floorEntry(time);
			float floorTime = floorEntry.getKey();
			Body floorBody = floorEntry.getValue();
			if (time == floorTime)
				return (Body) floorBody.clone();
			Map.Entry<Float, Body> ceilingEntry = bodies.ceilingEntry(time);
			float ceilingTime = ceilingEntry.getKey();
			Body ceilingBody = ceilingEntry.getValue();
			Body result = Body.interpolate(floorBody, ceilingBody,
					(time - floorTime) / (ceilingTime - floorTime));
			return result;
		}
	}

	// returns the position of the body at this time (real or interpolated)
	public Body replayFrame(float deltaTime)
	{
		float replay_time = getReplayTime();

		percentPassed = replay_time / duration;

		// System.out.println(totalTime);

		Body response = at(replay_time);

		response.setTimestamp(totalTime);
		totalTime += replaySpeed * deltaTime;
		// the position of the body corresponding to the given time is returned
		return response;
	}

	private float getReplayTime()
	{
		if (!isRepetitionOn)
		{
			if (totalTime > duration)
			{
				return duration;
			}
			else
			{
				return totalTime;
			}
		}
		else
		{
			int nb_cycles = (int) (totalTime / duration);
			float endOfLastCycle = duration * nb_cycles;
			float timeInCurrentCycle = totalTime - endOfLastCycle;
			if (isDirectRepetition || 0 == nb_cycles % 2)
			{
				return timeInCurrentCycle;
			}
			else
			{
				return duration - timeInCurrentCycle;
			}
		}
	}

	public float percentPassed()
	{
		return percentPassed;
	}

	public float getBasicDuration()
	{
		return bodies.lastKey();
	}

	public float getTimePlayed()
	{
		return totalTime;
	}

	public float getTimeInCurrentCycle()
	{
		int nb_cycles = (int) (totalTime / duration);
		float endOfLastCycle = duration * nb_cycles;
		float timeInCurrentCycle = totalTime - endOfLastCycle;
		return timeInCurrentCycle;
	}

	public static TempoIndependentMovement load(String filename)
	{
		TempoIndependentMovement readMovement = null;
		FileInputStream fileinstream = null;
		ObjectInputStream objectinstream = null;
		try
		{
			File infile = new File(filename);
			fileinstream = new FileInputStream(infile);

			if (fileinstream.available() == 0)
			{
				fileinstream.close();
				return null;
			}

			objectinstream = new ObjectInputStream(fileinstream);
			readMovement = (TempoIndependentMovement) objectinstream.readObject();
			objectinstream.close();
		}
		catch (IOException | ClassNotFoundException e)
		{
			// System.out.println("Exception in deserializeTempoIndependentMovement");
			 e.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				if (objectinstream != null)
				{
					objectinstream.close();
				}
				if (fileinstream != null)
				{
					fileinstream.close();
				}
			}
			catch (Exception e)
			{
				System.out.println("Exception in finally of deserializeTempoIndependentMovement");
				e.printStackTrace();
				return null;
			}
		}

		// System.out.println("Done Reading Joints Gesture: " + path);
		if (readMovement == null)
		{
			return null;
		}
		return readMovement;
	}

	public static void save(String path, TempoIndependentMovement movement)
	{
		FileOutputStream fileoutstream = null;
		ObjectOutputStream objectoutstream = null;
		try
		{
			fileoutstream = new FileOutputStream(path);
			objectoutstream = new ObjectOutputStream(fileoutstream);
			objectoutstream.writeObject(movement);
			objectoutstream.flush();
			objectoutstream.close();
			fileoutstream.flush();
			fileoutstream.close();
		}
		catch (IOException e)
		{
			// System.out.println("Exception in save");
			 e.printStackTrace();
		}
		finally
		{
			try
			{
				if (objectoutstream != null)
				{
					objectoutstream.close();
				}
				if (fileoutstream != null)
				{
					fileoutstream.close();
				}
			}
			catch (Exception e)
			{
				System.out.println("Exception in finally of save");
				e.printStackTrace();
			}
		}

		// System.out.println("Done Writing Tempo Independent Movement: " +
		// path);
	}
	
	public ArrayList<Body> getBodies(int count)
	{
		ArrayList<Body> bodies = new ArrayList<Body>();
		for(int i = 0; i < count; i++)
		{
			bodies.add(at(1f * i * getBasicDuration() / (count - 1)));
		}
		
		return bodies;
	}

	public UUID getID()
	{
		return ID;
	}

	public void setID(UUID iD)
	{
		ID = iD;
	}
}