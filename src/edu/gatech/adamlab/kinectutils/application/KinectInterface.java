package edu.gatech.adamlab.kinectutils.application;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import SimpleOpenNI.SimpleOpenNI;
import edu.gatech.adamlab.kinectutils.io.socket.SocketConstants;
import edu.gatech.adamlab.kinectutils.io.socket.perception.PerceptionSocketOutputData;
import edu.gatech.adamlab.kinectutils.io.socket.shared.BasicSocket;
import edu.gatech.adamlab.kinectutils.io.socket.shared.BasicSocket.TYPE;
import edu.gatech.adamlab.kinectutils.motioninput.FileMotionInput;
import edu.gatech.adamlab.kinectutils.motioninput.KinectMotionInput;
import edu.gatech.adamlab.kinectutils.motioninput.MotionInput;
import edu.gatech.adamlab.kinectutils.shared.Constants.JPIDX;
import edu.gatech.adamlab.kinectutils.shared.Body;
import edu.gatech.adamlab.kinectutils.shared.Clock;
import edu.gatech.adamlab.kinectutils.shared.LIDX;
import edu.gatech.adamlab.kinectutils.shared.Pair;
import edu.gatech.adamlab.kinectutils.shared.TempoIndependentMovement;
import processing.core.PApplet;
import processing.core.PVector;

public class KinectInterface extends PApplet
{
	/**
	 * Default generated Serial Version UID. Please do not change.
	 */
	private static final long serialVersionUID = 5563536837918268812L;
	
	private static final Boolean isFullScreen = true;
	private static Boolean USE_KINECT = true;
	private static Boolean DRAW_USER = true;
	private static Boolean DRAW_GUI = true;
	private static Boolean IS_RECORDING = false;
	private static final String CAPTURED_INPUT_SESSION_DIR = "data" + File.separator + "recorded-input-sessions"
			+ File.separator;
	private static String recordedInputSession = "input-session.session";

	private ArrayList<Body> currentMovement;
	private MotionInput motionInput = null;
	private KinectMotionInput kinectInput;
	private FileMotionInput fileInput;
	private float maxZ = 5000;
	private float minZ = 500;
	private Clock clock;
	private ArrayList<String> listGUIText;
	private ArrayList<Long> listGUIDuration;
	
	private BasicSocket perceptionPublisherSocket;

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		if (isFullScreen)
		{
			PApplet.main(new String[]
			{ "--present", "edu.gatech.adamlab.kinectutils.application.KinectInterface" });
		}
		else
		{
			PApplet.main(new String[]
			{ "edu.gatech.adamlab.kinectutils.application.KinectInterface" });
		}
	}

	//COMMENTED AND NOT REMOVED IN CASE PROCESSING 3.0+ IS EVER SUPPORTED BY SIMPLEOPENNI 1.96
//	public void settings()
//	{
//		if(isFullScreen)
//		{
//			setSize(displayWidth, displayHeight);
//		}
//		smooth();
//	}

	public void setup()
	{
		if(isFullScreen)
		{
			size(displayWidth, displayHeight);
		}
		smooth();
		
		clock = new Clock();
		clock.start();
		
		if (USE_KINECT)
		{
			if(kinectInput == null)
			{
				kinectInput = new KinectMotionInput(new SimpleOpenNI(this), minZ, maxZ);
			}
			
			if (kinectInput.init())
			{
				motionInput = kinectInput;
			}
		}
		else
		{
			if(fileInput == null)
			{
				fileInput = new FileMotionInput(CAPTURED_INPUT_SESSION_DIR + recordedInputSession);
			}
			
			if (fileInput.init())
			{
				motionInput = fileInput;
			}
		}
		
		listGUIText = new ArrayList<String>();
		listGUIDuration = new ArrayList<Long>();
		
		background(0, 0, 0);

		currentMovement = new ArrayList<Body>();
		
		perceptionPublisherSocket = new BasicSocket(SocketConstants.PERCEPTION_ADDRESS, TYPE.PUB, true);
	}

	public void draw()
	{
		if (motionInput == null || !motionInput.isInit())
		{
			return;
		}
		else
		{
			pushMatrix();
			
			clock.check();
			noStroke();
			fill(0, 0, 0, 150);
			rect(0, 0, displayWidth, displayHeight);

			if(DRAW_USER)
			{
				Body userPose = motionInput.next(clock);
				
				if(userPose != null)
				{
					//Sending the body to GENII for processing
					perceptionPublisherSocket.sendFiltered(new PerceptionSocketOutputData(userPose), PerceptionSocketOutputData.class, SocketConstants.PERCEPTION_FILTER, true);
					
					if (IS_RECORDING)
					{
						currentMovement.add(userPose);
					}
	
					if (DRAW_USER)
					{
						drawBody(userPose);
					}
				}
			}
			
			if (DRAW_GUI)
			{
				displayGUI();
			}
			
			popMatrix();
		}
	}

	public void drawBody(Body body)
	{
		pushMatrix();
		translate(width/2f, height/2f);
		int color = g.strokeColor;
		stroke(255);
		strokeWeight(2);

		for (Pair<JPIDX, JPIDX> limbEnds : LIDX.LIMB_ENDS().values())
		{
			PVector pos1 = getScreenPos(body.get(limbEnds.first));
			PVector pos2 = getScreenPos(body.get(limbEnds.second));
			line(pos1.x, pos1.y, pos2.x, pos2.y);
		}
		stroke(color);
		popMatrix();
	}

	private PVector getScreenPos(PVector pos)
	{
		PVector screenPos = PVector.mult(pos.get(), 0.3f); // new PVector();
		screenPos.x = -screenPos.x;
		screenPos.y = -screenPos.y;
		screenPos.z = (height / 900.0f) * 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	@SuppressWarnings("unused")
	private Body getScreenPos(Body pose)
	{
		Body screenPose = (Body) pose.clone();
		for (JPIDX jidx : JPIDX.ALL_JPIDX)
		{
			PVector tempVector = getScreenPos(screenPose.get(jidx).get());
			
			screenPose.set(jidx, tempVector.get());
		}
		
		return screenPose;
	}
	
	public void addGUIText(String message, long duration)
	{
		for (int index = 0; index < listGUIText.size(); index++)
		{
			String testMessage = listGUIText.get(index);
			if (message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0]))
			{
				listGUIText.remove(index);
				listGUIDuration.remove(index);
				break;
			}
		}

		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}

	public void displayGUI()
	{
		pushMatrix();
		int oldFill = g.fillColor;
		fill(255,255,255, 200);
		long timeNowMillis = System.currentTimeMillis();
		textSize(32);
		float xPosPercent = 2f / 100f * (float) displayWidth;
		float yPosPercent = 3f / 100f * (float) displayHeight;
		yPosPercent += 5f / 100f * (float) displayHeight;
		for (int index = 0; index < listGUIText.size(); index++)
		{
			String message = listGUIText.get(index);
			long displayTime = listGUIDuration.get(index);
			text(message, xPosPercent, yPosPercent, 75f);
			yPosPercent += 5f / 100f * (float) displayHeight;
			if (displayTime < timeNowMillis)
			{
				listGUIText.remove(index);
				listGUIDuration.remove(index);
			}
		}
		fill(oldFill);
		popMatrix();
	}

	public void onNewUser(SimpleOpenNI context, int userId)
	{
		System.out.println("onNewUser - userId: " + userId);
		context.startTrackingSkeleton(userId);
		kinectInput.getKinect().startTrackingSkeleton(userId);
	}

	public void onLostUser(SimpleOpenNI context, int userId)
	{
		System.out.println("onLostUser - userId: " + userId);
	}

	public void keyPressed()
	{
		if (key == 'u' || key == 'U')
		{
			DRAW_USER = !DRAW_USER;
			addGUIText("DRAW USER: " + DRAW_USER, 500);
		}

		if (key == 'r' || key == 'R')
		{
			if (!IS_RECORDING)
			{
				currentMovement.clear();
				IS_RECORDING = true;
			}
			else
			{
				IS_RECORDING = false;
				if (currentMovement != null && !currentMovement.isEmpty())
				{
					TempoIndependentMovement recordedSession = new TempoIndependentMovement(
							currentMovement);
					TempoIndependentMovement.save(CAPTURED_INPUT_SESSION_DIR
							+ UUID.randomUUID() + ".session", recordedSession);
				}
			}
			addGUIText("IS RECORDING: " + IS_RECORDING, 500);
		}
		
		if (key == 'k' || key == 'K')
		{
			USE_KINECT = !USE_KINECT;

			if (USE_KINECT)
			{
				if(kinectInput == null)
				{
					kinectInput = new KinectMotionInput(new SimpleOpenNI(this), minZ, maxZ);
				}
				
				if (!kinectInput.isInit())
				{
					kinectInput.init();

				}
				motionInput = kinectInput;
			}
			else
			{
				if(fileInput == null)
				{
					fileInput = new FileMotionInput(CAPTURED_INPUT_SESSION_DIR + recordedInputSession);
				}
				
				if (!fileInput.isInit())
				{
					fileInput.init();

				}
				motionInput = fileInput;
			}
			addGUIText("KINECT MODE: " + USE_KINECT, 500);
		}
		
		if(key == 'g' || key == 'G')
		{
			DRAW_GUI = !DRAW_GUI;
			addGUIText("DRAW_GUI: " + DRAW_GUI, 500);
		}
	}
}
