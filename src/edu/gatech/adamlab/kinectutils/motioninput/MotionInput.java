package edu.gatech.adamlab.kinectutils.motioninput;

import edu.gatech.adamlab.kinectutils.shared.Body;
import edu.gatech.adamlab.kinectutils.shared.Clock;

public interface MotionInput
{
	public boolean init();

	public boolean isInit();

	public Body next(Clock clock);
}
