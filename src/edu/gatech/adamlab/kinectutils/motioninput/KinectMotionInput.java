package edu.gatech.adamlab.kinectutils.motioninput;

import processing.core.*;

import java.util.HashMap;

import SimpleOpenNI.SimpleOpenNI;
import edu.gatech.adamlab.kinectutils.shared.Constants.JPIDX;
import edu.gatech.adamlab.kinectutils.shared.Body;
import edu.gatech.adamlab.kinectutils.shared.Clock;
import processing.core.PVector;

public class KinectMotionInput implements MotionInput
{
	private SimpleOpenNI kinect = null;
	private boolean isInit = false;
	private int userNum = -1;
	private float maxZ, minZ;

	public KinectMotionInput(SimpleOpenNI kinect)
	{
		this(kinect, 300, 5000);
	}

	public KinectMotionInput(SimpleOpenNI kinect, float minZ, float maxZ)
	{
		this.minZ = minZ;
		this.maxZ = maxZ;
		this.kinect = kinect;
	}

	public boolean init()
	{
		if (!kinect.init())
			return false;
		isInit = kinect.enableDepth();
		isInit = isInit && kinect.enableUser();
		return isInit;
	}

	public boolean isInit()
	{
		return isInit;
	}

	public boolean isAnyoneOnScreen(PGraphics pgraphics)
	{
		PImage userimage = kinect.userImage();
		userimage.loadPixels();
		for (int i = 0; i < userimage.pixels.length; i++)
		{
			int color = userimage.pixels[i];
			float r = pgraphics.red(color);
			float g = pgraphics.blue(color);
			float b = pgraphics.green(color);
			if (r != g || g != b)
			{
				return true;
			}
		}
		return false;
	}

	public Body next(Clock clock)
	{
		kinect.update();
		int[] userList = kinect.getUsers();
		int nearestI = -1;
		float nearestZ = Float.MAX_VALUE;
		for (int i = 0; i < userList.length; i++)
		{
			if (kinect.isTrackingSkeleton(userList[i]))
			{
				PVector jointPos = new PVector();
				kinect.getJointPositionSkeleton(userList[i],
						SimpleOpenNI.SKEL_NECK, jointPos);
				if (jointPos.z > minZ && jointPos.z < maxZ
						&& jointPos.z < nearestZ)
				{
					nearestI = i;
					nearestZ = jointPos.z;
				}
			}
		}
		if (nearestI > -1)
		{
			userNum = userList[nearestI];
			Body userPose = new Body();
			userPose.initialize(initializeJointPositions(kinect, userList[nearestI]), clock.time());
			return userPose;
		}
		return null;
	}
	
	private Float[] getFloatArrayFromPVector(PVector vector)
	{
		return (new Float[]{vector.x, vector.y, vector.z});
	}
	
	public HashMap<Integer, Float[]> initializeJointPositions(SimpleOpenNI context, int userId)
	{
		HashMap<Integer, Float[]> result = new HashMap<Integer, Float[]>();
		
		PVector jointPos = new PVector();

		// note: jointPos.get() is copying each PVector as they are
		// call-by-reference

		// Get Skeleton Coordinates
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_HEAD,
				jointPos);
		result.put(JPIDX.HEAD.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		// PApplet.print(joints.get(JPIDX.HEAD)+"\n");
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_NECK,
				jointPos);
		result.put(JPIDX.NECK.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos);
		// //Never used this before
		// result.put(JPIDX.LEFT_COLLAR.ordinal(),getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId,
				SimpleOpenNI.SKEL_LEFT_SHOULDER, jointPos);
		result.put(JPIDX.LEFT_SHOULDER.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_ELBOW,
				jointPos);
		result.put(JPIDX.LEFT_ELBOW.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_HAND,
				jointPos);
		result.put(JPIDX.LEFT_HAND.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId,
				SimpleOpenNI.SKEL_LEFT_FINGERTIP, jointPos); // always (0.0,
																// 0.0, 0.0)
		result.put(JPIDX.LEFT_FINGERTIP.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos);
		// //Never used this before
		// result.put(JPIDX.RIGHT_COLLAR.ordinal(),getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId,
				SimpleOpenNI.SKEL_RIGHT_SHOULDER, jointPos);
		result.put(JPIDX.RIGHT_SHOULDER.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW,
				jointPos);
		result.put(JPIDX.RIGHT_ELBOW.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_HAND,
				jointPos);
		result.put(JPIDX.RIGHT_HAND.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId,
				SimpleOpenNI.SKEL_RIGHT_FINGERTIP, jointPos); // always (0.0,
																// 0.0, 0.0)
		result.put(JPIDX.RIGHT_FINGERTIP.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_TORSO,
				jointPos);
		result.put(JPIDX.TORSO.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,jointPos);
		// // always (0.0, 0.0, 0.0)
		// result.put(JPIDX.WAIST.ordinal(),getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_HIP,
				jointPos);
		result.put(JPIDX.LEFT_HIP.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_KNEE,
				jointPos);
		result.put(JPIDX.LEFT_KNEE.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FOOT,jointPos);
		// // always (0.0, 0.0, 0.0)
		// result.put(JPIDX.LEFT_ANKLE.ordinal(),getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_LEFT_FOOT,
				jointPos);
		result.put(JPIDX.LEFT_FOOT.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_HIP,
				jointPos);
		result.put(JPIDX.RIGHT_HIP.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_KNEE,
				jointPos);
		result.put(JPIDX.RIGHT_KNEE.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		// context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FOOT,jointPos);
		// // always (0.0, 0.0, 0.0)
		// result.put(JPIDX.RIGHT_ANKLE.ordinal(),getFloatArrayFromPVector(jointPos.get()));
		context.getJointPositionSkeleton(userId, SimpleOpenNI.SKEL_RIGHT_FOOT,
				jointPos);
		result.put(JPIDX.RIGHT_FOOT.ordinal(), getFloatArrayFromPVector(jointPos.get()));
		
		return result;
	}

	public SimpleOpenNI getKinect()
	{
		return kinect;
	}

	public int getUser()
	{
		return userNum;
	}
}
